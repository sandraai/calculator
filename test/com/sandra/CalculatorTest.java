package com.sandra;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {
    // test adding
    @Test
    public void add_TwoPositiveIntegers_ReturnPositiveSum () {
        // arrange
        int testNumber1 = 10;
        int testNumber2 = 2;
        int expected = 12;
        Calculator calculator = new Calculator();

        // act
        int actual = calculator.add(testNumber1, testNumber2);

        // assert
        assertEquals(expected, actual);
    }

    @Test
    public void add_TwoNegativeDoubles_ReturnNegativeSum () {
        // arrange
        double testNumber1 = -10.56;
        double testNumber2 = -2.083;
        double expected = -12.643;
        Calculator calculator = new Calculator();

        // act
        double actual = calculator.add(testNumber1, testNumber2);

        // assert
        assertEquals(expected, actual);
    }


    // test subtracting
    @Test
    public void subtract_TwoPositiveIntegers_ReturnPositiveDifference () {
        // arrange
        int testNumber1 = 10;
        int testNumber2 = 2;
        int expected = 8;
        Calculator calculator = new Calculator();

        // act
        int actual = calculator.subtract(testNumber1, testNumber2);

        // assert
        assertEquals(expected, actual);
    }

    @Test
    public void subtract_TwoPositiveDoubles_ReturnNegativeDifference () {
        // arrange
        double testNumber1 = 2.083;
        double testNumber2 = 10.56;
        double expected = -8.477;
        Calculator calculator = new Calculator();

        // act
        double actual = calculator.subtract(testNumber1, testNumber2);

        // assert
        assertEquals(expected, actual);
    }


    // test multiplying
    @Test
    public void multiply_TwoPositiveIntegers_ReturnPositiveProduct () {
        // arrange
        int testNumber1 = 10;
        int testNumber2 = 2;
        int expected = 20;
        Calculator calculator = new Calculator();

        // act
        int actual = calculator.multiply(testNumber1, testNumber2);

        // assert
        assertEquals(expected, actual);
    }

    @Test
    public void multiply_OneNegativeOnePositiveDouble_ReturnNegativeProduct () {
        // arrange
        double testNumber1 = 10.56;
        double testNumber2 = -2.083;
        double expected = -21.996480000000002;
        Calculator calculator = new Calculator();

        // act
        double actual = calculator.multiply(testNumber1, testNumber2);

        // assert
        assertEquals(expected, actual);
    }


    // test dividing
    @Test
    public void divide_TwoPositiveIntegers_ReturnPositiveFraction () throws InvalidCalculatorInputException {
        // arrange
        int testNumber1 = 10;
        int testNumber2 = 2;
        int expected = 5;
        Calculator calculator = new Calculator();

        // act
        int actual = calculator.divide(testNumber1, testNumber2);

        // assert
        assertEquals(expected, actual);
    }

    @Test
    public void divide_TwoPositiveDoubles_ReturnPositiveFraction () throws InvalidCalculatorInputException {
        // arrange
        double testNumber1 = 2;
        double testNumber2 = 10;
        double expected = 0.2;
        Calculator calculator = new Calculator();

        // act
        double actual = calculator.divide(testNumber1, testNumber2);

        // assert
        assertEquals(expected, actual);
    }

    @Test
    public void divide_DivisorIsZero_ThrowsException () {
        // arrange
        int testNumber1 = 10;
        int testNumber2 = 0;
        Calculator calculator = new Calculator();

        // act and assert
        assertThrows(InvalidCalculatorInputException.class, () -> calculator.divide(testNumber1, testNumber2));
    }
}