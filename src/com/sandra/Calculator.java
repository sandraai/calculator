package com.sandra;

public class Calculator {

    public int add (int term1, int term2) {

        return term1 + term2;
    }
    public double add (double term1, double term2) {
        return term1 + term2;
    }

    public int subtract(int term1, int term2) {

        return term1 - term2;
    }

    public double subtract(double term1, double term2) {

        return term1 - term2;
    }

    public int multiply(int factor1, int factor2) {
        return factor1 * factor2;
    }

    public double multiply(double factor1, double factor2) {
        return factor1 * factor2;
    }

    public int divide(int dividend, int divisor) throws InvalidCalculatorInputException {
        if (divisor == 0) {
            throw new InvalidCalculatorInputException();
        }
        return dividend / divisor;
    }

    public double divide(double dividend, double divisor) throws InvalidCalculatorInputException {
        if (divisor == 0) {
            throw new InvalidCalculatorInputException();
        }
        return dividend / divisor;
    }
}
